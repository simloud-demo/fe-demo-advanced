import reactRefresh from '@vitejs/plugin-react-refresh'
import { defineConfig, loadEnv } from 'vite'
import eslintPlugin from 'vite-plugin-eslint'
import tsconfigPaths from 'vite-tsconfig-paths'
import mkcert from 'vite-plugin-mkcert'
import basicSsl from '@vitejs/plugin-basic-ssl'

const config = ({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) }

  return defineConfig({
    build: {
      chunkSizeWarningLimit: 1200,
      outDir: 'build'
    },
    plugins:
      mode === 'production'
        ? [eslintPlugin(), tsconfigPaths()]
        : [basicSsl(), mkcert(), eslintPlugin(), tsconfigPaths(), reactRefresh()],
    css: {
      preprocessorOptions: {
        less: {
          javascriptEnabled: true
        }
      }
    },
    server: {
      https: true,
      // host: `local.${process.env.VITE_HOST_DOMAIN}`,
      port: 443
    }
  })
}

export default config
