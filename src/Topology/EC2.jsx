import React from 'react'
import Layer from './Layer'

const EC2 = ({ size, type }) => {
  return (
    <>
      <Layer img="ec2" />
      <Layer img={`ec2/ec2-size-${size}`} />
      <Layer img={`ec2/ec2-type-${type}`} />
    </>
  )
}

export default EC2
