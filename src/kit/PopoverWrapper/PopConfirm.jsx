import React from 'react'
import Tippy from '@tippyjs/react'
import Button from 'kit/Button'
import { TIPPY_EVENTS } from '@/constants'

const PopConfirm = React.forwardRef(({ popConfirm, cmp, onClick, ...rest }, ref) => {
  const [isVisible, setIsVisible] = React.useState(false)

  if (!popConfirm.content) throw new Error('popConfirm _content_ is required')
  if (!popConfirm.onConfirm) throw new Error('popConfirm _onConfirm_ is required')

  const tippyEvents = Object.keys(popConfirm)
    .filter(propKey => TIPPY_EVENTS.includes(propKey))
    .reduce((acc, propKey) => {
      acc[propKey] = popConfirm[propKey]

      return acc
    }, {})

  return (
    <Tippy
      content={
        <>
          <div className="p-4">{popConfirm.content}</div>
          <div className="py-2 px-3 flex space-x-2 justify-end bg-gray-50">
            {popConfirm.noBtn ? (
              React.cloneElement(popConfirm.noBtn, {
                onClick: props => {
                  setIsVisible(false)
                  popConfirm.noBtn.props?.onClick?.(props)
                }
              })
            ) : (
              <Button onClick={() => setIsVisible(false)}>Cancel</Button>
            )}
            {popConfirm.yesBtn ? (
              React.cloneElement(popConfirm.yesBtn, {
                onClick: props => {
                  setIsVisible(false)
                  popConfirm.yesBtn.props?.onClick?.(props)
                }
              })
            ) : (
              <Button
                onClick={() => {
                  popConfirm.onConfirm()
                  setIsVisible(false)
                }}
                type="primary"
              >
                Yes
              </Button>
            )}
          </div>
        </>
      }
      placement={popConfirm.placement || 'left'}
      appendTo={() => popConfirm.elementAppendTo || document.body}
      visible={isVisible}
      delay={0}
      arrow={popConfirm.arrow !== undefined ? popConfirm.arrow : true}
      duration={0}
      onHidden={() => setIsVisible(false)}
      onClickOutside={() => setIsVisible(false)}
      offset={popConfirm.offset || [0, 5]}
      interactive={true}
      className="popconfirm-default"
      {...tippyEvents}
    >
      {React.cloneElement(cmp, {
        ref,
        onClick: props => {
          setIsVisible(true)
          onClick?.(props)
        },
        ...rest
      })}
    </Tippy>
  )
})

export default PopConfirm
