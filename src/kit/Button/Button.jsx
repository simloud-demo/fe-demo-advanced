import React from 'react'
import Icon from 'kit/Icon'
import PopoverWrapper from 'kit/PopoverWrapper'

const ButtonCmp = React.forwardRef(
  (
    {
      children,
      type = 'default', // 'default', 'primary', 'danger', 'link'
      iconClassName = '',
      disabled = false,
      isLoading = false,
      loadingText = 'Loading...',
      leadingIconName = null,
      trailingIconName = null,
      onClick = null,
      className = '',
      ...rest
    },
    ref
  ) => {
    const defaultStyles = 'text-gray-600 bg-white hover:bg-gray-100 font-medium'
    const disabledStyles =
      'border-gray-200 text-gray-300 bg-gray-50 cursor-not-allowed shadow-none font-medium'
    const dangerStyles =
      'border-transparent text-white bg-red-500 hover:bg-red-600 font-medium'
    const primaryStyles =
      'border-transparent text-white bg-blue-500 hover:bg-blue-600 font-medium'
    const linkStyles = 'border-none p-0 text-blue-500 hover:text-blue-700 shadow-none'
    const loadingStyles = '!text-gray-400 bg-gray-50 border-none cursor-wait'

    return (
      <button
        ref={ref}
        type="button"
        className={`button-default group ${
          disabled
            ? disabledStyles
            : type === 'danger'
            ? dangerStyles
            : type === 'primary'
            ? primaryStyles
            : type === 'link'
            ? linkStyles
            : defaultStyles
        } ${isLoading ? loadingStyles : ''} ${className} ${
          leadingIconName ? 'pl-2.5 pr-3' : ''
        } ${trailingIconName ? 'pr-2.5 pl-3' : ''}`}
        onClick={disabled || isLoading || !onClick ? () => {} : onClick}
        {...rest}
      >
        {isLoading ? (
          <>
            <Icon
              {...{
                name: 'Spin',
                className: type !== 'default' && !disabled ? 'text-white' : ''
              }}
            />
            <span>{loadingText}</span>
          </>
        ) : (
          <>
            {leadingIconName && (
              <Icon
                {...{
                  name: leadingIconName,
                  className: `${disabled ? 'text-gray-300' : iconClassName}`
                }}
              />
            )}
            {children && <span className="flex items-center">{children}</span>}
            {trailingIconName && (
              <Icon
                {...{
                  name: trailingIconName,
                  className: `h-4 w-4 ${disabled ? 'text-gray-300' : iconClassName}`
                }}
              />
            )}
          </>
        )}
      </button>
    )
  }
)

const Button = props => <PopoverWrapper {...props} {...{ cmp: <ButtonCmp /> }} />

export default Button
