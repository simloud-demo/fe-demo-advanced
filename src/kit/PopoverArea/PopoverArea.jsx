import React from 'react'
import PopoverWrapper from 'kit/PopoverWrapper'

const PopoverAreaCmp = React.forwardRef(({ children, className = '' }, ref) => {
  return (
    <span ref={ref} className={className}>
      {children}
    </span>
  )
})

const PopoverArea = props => (
  <PopoverWrapper {...props} {...{ cmp: <PopoverAreaCmp /> }} />
)

export default PopoverArea
