import React from 'react'
import useDebouncedSave from '@/hooks/useDebouncedSave'

const Input = ({
  isLoading = false,
  disabled = false,
  className = '',
  value = '',
  onBlur = () => {},
  onChange = () => {},
  ...rest
}) => {
  const { inputText, setInputText } = useDebouncedSave(newVal => onChange(newVal))

  React.useEffect(() => {
    setInputText(value)
  }, [value])

  const defaultStyles = 'border-gray-300 bg-white text-gray-900 placeholder-gray-500'
  const disabledStyles =
    'border-gray-300 bg-gray-100 text-gray-500 cursor-not-allowed shadow-none placeholder-gray-400'

  if (isLoading)
    return (
      <input
        type="text"
        disabled={true}
        className={`input-default ${disabledStyles} ${className}`}
        value="Loading..."
        {...rest}
      />
    )

  return (
    <input
      type="text"
      disabled={disabled}
      className={`input-default ${
        disabled ? disabledStyles : defaultStyles
      } ${className}`}
      value={inputText}
      onChange={e => setInputText(e.target.value)}
      onBlur={e => setInputText(e.target.value)}
      {...rest}
    />
  )
}

export default Input
